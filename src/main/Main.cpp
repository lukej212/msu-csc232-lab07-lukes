/**
 *
 * @authors: Jacob Wakefield <wakefield515@live.missouristate.edu> 
 *           Luke Johnson <edward212@live.missouristate.edu> 
 *           Geoffrey Lewis <lew1996@live.missouristate.edu>
 *
 *
 *
**/
#include <iostream>
#include <string>

bool isWord(std::string);
bool strIsInL(std::string);

int main() {
    std::cout << "abb" << std::endl;
    std::cout << "aabbbb" << std::endl;
    std::cout << "$" << std::endl;
    std::cout << "$$" << std::endl;
    std::cout << "$$$" << std::endl;
    std::cout << "$$$$" << std::endl;
    std::cout << "$$$$$" << std::endl;
    std::cout << "$$$$$$" << std::endl;
    std::cout << "$$$$$$$" << std::endl;
    std::cout << "$abb" << std::endl;
    std::cout << "$$abb" << std::endl;
    std::cout << "$$$abb" << std::endl;
    std::cout << "$$$$abb" << std::endl;
    std::cout << "$aabbbb" << std::endl;
    return 0;
}

/**
 * This function is a solution to Exercise 6, Chapter 5.
 * @param word a potential word in the language described in Exercise 6
 * @return true if the string is in the given language, false otherwise.
 */

 bool isWord(std::string word) {

    if(word == "."){
        return true;
    }
    else if((word.substr(0, 1) == "-") && (word.length() > 1)){ //checks firts char and length of string
        return isWord(word.substr(1)); //passes word minus its first char to isWord()
    }
    else if((word.substr(0, 1) == ".") && (word.find("-") == std::string::npos) && (word.length() > 1)){ //checks first char and length of string
        return isWord(word.substr(1)); //passes word minus its first char to isWord()
    }
    else{
        return false;
    }
}

/**
 * This function is a solution to Exercise 9, Chapter 5.
 * @param s a potential string in language L
 * @return true if the given string is valid in L, false otherwise
 */
bool strIsInL(std::string s) {
    if(s == "ABB"){
        return true;
    }
    else if((s.substr(0,1) == "A") && (s.substr(s.length() - 2) =="BB")){
        return strIsInL(s.substr(1,s.length() -3));
    }    
    else{
        return false;
    }
}
